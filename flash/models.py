from django.db import models

class Topic(models.Model):
    topic_name = models.CharField(max_length=50, unique=True)
    week_number = models.IntegerField(default=99)
    topic_short = models.CharField(max_length=500, default=topic_name)

    def __str__(self):
        return f'Week {self.week_number}: {self.topic_name}'


class Question(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    question_text = models.CharField(max_length=500)
    answer_text = models.TextField()

    def __str__(self):
        return self.question_text
