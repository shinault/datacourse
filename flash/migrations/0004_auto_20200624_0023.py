# Generated by Django 3.0.5 on 2020-06-24 00:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flash', '0003_topic_topic_short'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topic',
            name='topic_short',
            field=models.CharField(default=models.CharField(max_length=50, unique=True), max_length=500),
        ),
    ]
