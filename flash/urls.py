from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('topic/<str:name>/', views.view_topic, name='view_topic'),
]
