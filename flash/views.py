from django.shortcuts import render

from .models import Question, Topic

def index(request):
    topics = sorted(Topic.objects.all(), key=lambda x: x.week_number)
    return render(request, 'home.html', {'topics': topics})

def view_topic(request, name):
    topic = Topic.objects.get(topic_short__iexact=name)
    topics = sorted(Topic.objects.all(), key=lambda x: x.week_number)
    questions = topic.question_set.all()
    return render(request, 'topic.html', {'topics': topics, 'topic': topic, 'questions': questions})
